import React from "react";
import MyResponsivePie from './components/MyResponsivePie';


const App = () => (
    <div className="d-flex flex-column align-items-center justify-content-center mt-5 h-100">
        <h1>React Graphing Test</h1>
        <MyResponsivePie />
    </div>
);

export default App;