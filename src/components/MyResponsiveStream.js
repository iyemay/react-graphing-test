import React, { Component } from 'react';
import { ResponsiveStream } from '@nivo/stream';
import data from './data';

export default class MyResponsiveStream extends Component {
    render() {
        return (
            <div>
                <div className="chartWrapper" style={{ height: 300 }}>
                    <ResponsiveStream
                        data={data}
                        keys={["Raoul", "Josiane", "Marcel", "René", "Paul", "Jacques"]}
                        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            orient: "bottom",
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: "",
                            legendOffset: 36,
                            tickValues: ["A", "B", "C"]
                        }}
                        axisLeft={{
                            orient: "left",
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            legend: "",
                            legendOffset: -40
                        }}
                        offsetType="silhouette"
                        colors={{ scheme: "nivo" }}
                        fillOpacity={0.85}
                        borderColor={{ theme: "background" }}
                        dotSize={8}
                        dotColor={{ from: "color" }}
                        dotBorderWidth={2}
                        dotBorderColor={{ from: "color", modifiers: [["darker", 0.7]] }}
                        animate={true}
                        motionStiffness={90}
                        motionDamping={15}
                        legends={[
                            {
                                anchor: "bottom-right",
                                direction: "column",
                                translateX: 100,
                                itemWidth: 80,
                                itemHeight: 20,
                                itemTextColor: "#999999",
                                symbolSize: 12,
                                symbolShape: "circle",
                                effects: [
                                    {
                                        on: "hover",
                                        style: {
                                            itemTextColor: "#000000"
                                        }
                                    }
                                ]
                            }
                        ]}
                    />
                </div>
            </div>
        );
    }
}
